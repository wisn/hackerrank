#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

#define repeat(i, n) for (int i = 0; (i) < int(n); ++(i))
#define walk(i, n) for (auto it = i; (it) != (n); ++(it))

typedef pair<int, int> iPair;

class Graph {
  int v, e;
  vector<pair<int, iPair>> wgh;
public:
  Graph(int v, int e);
  void addEdge(int u, int v, int w);
  int kruskalMST();
};

Graph::Graph(int v, int e) {
  this->v = v;
  this->e = e;
}

void Graph::addEdge(int u, int v, int w) {
  wgh.push_back({w, {u, v}});
}

class DisjointSets {
  int n, *parent, *rank;
public:
  DisjointSets(int n);
  int find(int u);
  void merge(int x, int y);
};

DisjointSets::DisjointSets(int n) {
  this->n = n;

  parent = new int[n + 1];
  rank   = new int[n + 1];

  repeat(i, n + 1)
  {
    rank[i]   = 0;
    parent[i] = i;
  }
}

int DisjointSets::find(int u) {
  if (u != parent[u])
    parent[u] = find(parent[u]);

  return parent[u];
}

void DisjointSets::merge(int x, int y) {
  x = find(x);
  y = find(y);

  if (rank[x] > rank[y])
    parent[y] = x;
  else
    parent[x] = y;

  if (rank[x] == rank[y]) rank[y]++;
}

int Graph::kruskalMST() {
  int w = 0;

  sort(wgh.begin(), wgh.end());

  DisjointSets ds(v);

  vector<pair<int, iPair>>::iterator it;
  walk (wgh.begin(), wgh.end())
  {
    int u = it->second.first,
        v = it->second.second;

    int set_u = ds.find(u),
        set_v = ds.find(v);

    if (set_u != set_v)
    {
      w += it->first;
      ds.merge(set_u, set_v);
    }
  }

  return w;
}

int main() {
  short n;
  int m, r;
  cin >> n >> m;
  Graph g(n, m);

  short u, v;
  while (m--)
  {
    cin >> u >> v >> r;
    g.addEdge(u, v, r);
  }

  int w = g.kruskalMST();
  printf("%d\n", w);

  return 0;
}

