#include <vector>
#include <iostream>
using namespace std;

struct Node {
  int parent, child;

  Node(int p, int c) {
    this->child = c;
    this->parent = p;
  }
};

struct Tree {
  vector<Node*> N;

  Tree(int n) {
    while (n--)
    {
      Node* n = new Node(-1, 1);

      this->N.push_back(n);
    }
  }

  void connect(int u, int v) {
    this->N[u]->parent = v;
  }

  int evenTree() {
    int treeSize = this->N.size();

    for (int i = treeSize - 1; i > 0; i--)
      if (this->N[i]->parent > -1)
        this->N[this->N[i]->parent]->child += this->N[i]->child;

    int result = 0;
    for (int i = 0; i < treeSize; i++)
      if (this->N[i]->parent > -1 && this->N[i]->child % 2 == 0)
        result++;

    return result;
  }
};

int main() {
  int n, m;
  scanf("%d %d", &n, &m);

  Tree t (n);

  while (m--)
  {
    int u, v;
    scanf("%d %d", &u, &v);

    t.connect(--u, --v);
  }

  int result = t.evenTree();

  printf("%d\n", result);

  return 0;
}
