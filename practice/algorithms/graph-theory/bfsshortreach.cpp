#include <list>
#include <vector>
#include <iostream>
using namespace std;

struct Edge {
  int node, weight;
};

struct Graph {
  int V;
  list<Edge>* E;
  vector<int> R;
  vector<bool> visited;

  Graph(int n) {
    this->V = n;
    this->E = new list<Edge>[n];

    vector<int> R(n, 0);
    this->R = R;

    vector<bool> visited(n, false);
    this->visited = visited;
  }

  void addEdge(int u, int v) {
    Edge e;
    e.weight = 6;

    e.node = v;
    this->E[u].push_back(e);

    e.node = u;
    this->E[v].push_back(e);
  }

  void BFS(int s) {
    list<int> q;
    q.push_back(s);

    this->visited[s] = true;

    list<Edge>::iterator it;

    while (!q.empty())
    {
      int s = q.front();
      for (it = this->E[s].begin(); it != this->E[s].end(); it++)
      {
        if (!this->visited[it->node])
        {
          q.push_back(it->node);
          this->visited[it->node] = true;
          this->R[it->node] += this->R[s] + it->weight;
        }
      }

      q.pop_front();
    }
  }

  void result(int s) {
    for (int i = 0; i < this->V; i++)
    {
      if (i != s)
      {
        printf("%d", this->R[i] == 0 ? -1 : this->R[i]);

        if (i < this->V - 1) printf(" ");
      }
    }
    printf("\n");
  }

  // DEBUG ONLY
  void show() {
    for (int i = 0; i < this->V; i++)
    {
      printf("%d", i);
      for (Edge e : this->E[i]) printf(" -> %d [%d]", e.node, e.weight);
      printf("\n");
    }
  }
};

int main() {
  short q;
  cin >> q;

  while (q--)
  {
    int m;
    short n, u, v, s;

    cin >> n >> m;

    Graph g(n);

    while (m--)
    {
      cin >> u >> v;
      g.addEdge(--u, --v);
    }

    cin >> s;

    g.BFS(--s);
    g.result(s);
  }

  return 0;
}

