#include <iostream>
#include <vector>

using namespace std;

#define ll long long

struct Node {
  vector<int> ne;
  int money = 0;
};

int getMoney(int cur, bool con, ll& count, vector<Node>& n, vector<int>& s) {
  s[cur] = 2;
  int money = 0;

  if (con)
  {
    money += n[cur].money;
    for (auto ne : n[cur].ne) if (s[ne] == 0) s[ne] = 1;
  }

  for (auto ne : n[cur].ne)
  {
    if (s[ne] == 2)
      continue;
    else if (s[ne] == 1)
      money += getMoney(ne, false, count, n, s);
    else
    {
      ll c1 = 1, c2 = 1;
      auto cp = s;
      auto p1 = getMoney(ne, false, c1, n, cp);
      auto p2 = getMoney(ne, true, c2, n, s);

      if (p1 == p2)
      {
        money += p1; count *= (c1 + c2);
      }
      else if (p1 > p2)
      {
        money += p1; count *= c1;
      }
      else
      {
        money += p2; count *= c2;
      }
    }
  }

  return money;
}

int main() {
  int n, m;
  cin >> n >> m;

  vector<Node> nodes(n + 1);
  for (int i = 0; i < n; i++) cin >> nodes[i].money;

  for (int i = 0; i < m; i++)
  {
    int u, v;
    cin >> u >> v;

    nodes[u - 1].ne.push_back(v - 1);
    nodes[v - 1].ne.push_back(u - 1);
  }

  ll count = 1;
  vector<int> s(n + 1);
  for (int i = 0; i < n; i++)
    nodes[n].ne.push_back(i);

  auto money = getMoney(n, false, count, nodes, s);
  printf("%d %lld\n", money, count);

  return 0;
}

