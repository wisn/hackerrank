#include <iostream>
using namespace std;

int main() {
  short a[3], b[3], i;
  for (i = 0; i < 3; i++) cin >> a[i];
  for (i = 0; i < 3; i++) cin >> b[i];

  short alice = 0, bob = 0;
  for (i = 0; i < 3; i++)
  {
      if (a[i] > b[i]) alice++;
      else if (b[i] > a[i]) bob++;
  }

  printf("%d %d\n", alice, bob);

  return 0;
}

