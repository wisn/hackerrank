#include <iostream>
using namespace std;

int main() {
  short i, n = 5;
  long long input[5];
  for (i = 0; i < n; i++) cin >> input[i];

  short j;
  long long sum[5];
  for (i = 0; i < n; i++)
  {
    sum[i] = 0;
    for (j = 0; j < n; j++)
    {
      if (i != j) sum[i] += input[j];
    }
  }

  long long min = sum[0], max = sum[0];
  for (long long v : sum)
  {
    if (v > max) max = v;
    if (v < min) min = v;
  }

  printf("%lld %lld\n", min, max);

  return 0;
}

