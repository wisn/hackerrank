#include <iostream>
using namespace std;

int main() {
  short n;
  cin >> n;

  int sum = 0, x;
  while (n--)
  {
      cin >> x; sum += x;
  }

  cout << sum << endl;

  return 0;
}

