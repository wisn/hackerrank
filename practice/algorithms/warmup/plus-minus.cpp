#include <iostream>
using namespace std;

int main(){
  short t;
  cin >> t;

  short x, i;
  float p = 0, n = 0, z = 0;
  for (i = 0; i < t; i++)
  {
    cin >> x;
    if (x > 0) p++;
    else if (x < 0) n++;
    else z++;
  }

  p /= t; n /= t; z /= t;

  printf("%.6f\n%.6f\n%.6f\n", p, n, z);

  return 0;
}

