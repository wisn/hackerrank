#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

int main() {
  int n;
  cin >> n;

  int i;
  vector <int> candles (n);
  for (i = 0; i < n; i++) cin >> candles[i];

  auto result = max_element(candles.begin(), candles.end());
  int max = candles[distance(candles.begin(), result)];
  int sum = 0;
  for (i = 0; i < n; i++)
  {
    if (candles[i] == max) sum++;
  }

  printf("%d\n", sum);

  return 0;
}

