#include <iostream>
using namespace std;

int main() {
  short n;
  cin >> n;

  short m[n][n], i, j;
  for (i = 0; i < n; i++)
  {
    for (j = 0; j < n; j++) cin >> m[i][j];
  }

  short a = 0, b = 0;
  for (i = 0; i < n; i++) a += m[i][i];

  j = 0;
  for (i = n - 1; i >= 0; i--)
  {
    b += m[j][i]; j++;
  }
 
  short res = abs(a - b);
  cout << res << endl;

  return 0;
}

