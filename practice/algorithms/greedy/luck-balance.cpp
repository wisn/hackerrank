#include <iostream>
#include <algorithm>
#include <vector>
using namespace std;

struct Comp {
  int l, t;

  bool operator()(const Comp& c, const Comp& d) const{
    return c.l > d.l;
  }
};

int main() {
  int n, k;
  cin >> n >> k;

  vector<Comp> c(n);
  for (int i = 0; i < n; i++) cin >> c[i].l >> c[i].t;

  sort(c.begin(), c.end(), Comp());

  int sum = 0;
  for (int i = 0; i < n; i++)
  {
    if (c[i].t == 0) sum += c[i].l;
    else
    {
      if (k > 0)
      {
        sum += c[i].l; k--;
      }
      else sum -= c[i].l;
    }
  }

  printf("%d\n", sum);

  return 0;
}

