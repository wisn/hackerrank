#include<map>
#include <iostream>
using namespace std;

int main() {
  int q;
  cin >> q;

  map<int, int> heap;

  int v;
  short t;
  while (q--)
  {
    cin >> t;

    if (t < 3) {
      cin >> v;

      if (t == 1)
        heap.insert(make_pair(v, 1));
      else
        heap[v] = 0;
    }
    else
    {
      map<int, int>::iterator it;

      for (it = heap.begin(); it != heap.end(); it++)
      {
        if (it->second > 0)
        {
          printf("%d\n", it->first); break;
        }
      }
    }
  }
  
  return 0;
}

